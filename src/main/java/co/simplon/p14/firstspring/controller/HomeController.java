package co.simplon.p14.firstspring.controller;

import java.time.LocalDate;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.p14.firstspring.entity.Dog;
import co.simplon.p14.firstspring.repository.DogRepository;

@RestController
public class HomeController {
    
    @GetMapping
    public String firstRoute() {
        return "hola";
    }

    @GetMapping("/first-dog")
    public Dog showDog() {
        return new Dog(1, "Fido", "corgi", LocalDate.now());
    }
    @PostMapping("/add-dog")
    public Dog addDog(@RequestBody @Valid Dog dog) {

        System.out.println(dog.getName());

        
        
        return dog;
    }

    @GetMapping("/first-dog/{id}")
    public String showDogId(@PathVariable int id) {
        return "you entered "+id;
    }
    
    
}

