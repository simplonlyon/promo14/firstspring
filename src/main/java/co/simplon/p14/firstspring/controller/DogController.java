package co.simplon.p14.firstspring.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p14.firstspring.entity.Dog;
import co.simplon.p14.firstspring.repository.DogRepository;

@RestController
@RequestMapping("/api/dog")
public class DogController {
    @Autowired
    private DogRepository repo;

    @GetMapping
    public List<Dog> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Dog one(@PathVariable int id) {
        Dog dog = repo.findById(id);
        if(dog == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);//404
        }
        return dog;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)//204
    public void delete(@PathVariable int id) {
        if(!repo.delete(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{id}")
    public Dog update(@PathVariable int id, @RequestBody Dog dog) {
        Dog found = repo.findById(id);
        if(found == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        //ça, pas ouf, ptêt moyen de faire mieux, mais je trouve pas d'équivalent à 
        //Object.assign....
        if(dog.getName() != null && !dog.getName().isEmpty()) {
            found.setName(dog.getName());
        }
        if(dog.getBreed() != null && !dog.getBreed().isEmpty()) {
            found.setBreed(dog.getBreed());
        }

        if(dog.getBirthdate() != null) {
            found.setBirthdate(dog.getBirthdate());
        }
        repo.update(found);
        return found;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)//201
    public Dog addDog(@RequestBody @Valid Dog dog) {
        repo.add(dog);
        return dog;
    }

}
