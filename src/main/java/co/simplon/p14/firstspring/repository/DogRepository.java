package co.simplon.p14.firstspring.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.p14.firstspring.entity.Dog;

@Repository
public class DogRepository {
    @Autowired
    private DataSource dataSource;

    public List<Dog> findAll() {
        List<Dog>  list = new ArrayList<>();

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog");
            
            ResultSet result = stmt.executeQuery();
            while(result.next()) {
                Dog dog = new Dog(
                    result.getInt("id"),
                    result.getString("name"), 
                    result.getString("breed"), 
                    result.getDate("birthdate").toLocalDate());
                list.add(dog);
            }
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return list;
    }

    public Dog findById(int id) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog WHERE id=?");
            stmt.setInt(1, id);
            
            ResultSet result = stmt.executeQuery();
            if(result.next()) {
                Dog dog = new Dog(
                    result.getInt("id"),
                    result.getString("name"), 
                    result.getString("breed"), 
                    result.getDate("birthdate").toLocalDate());
                return dog;
            }
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return null;
    }
    public boolean delete(int id) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM dog WHERE id=?");
            stmt.setInt(1, id);
            
            return stmt.executeUpdate() == 1;
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;
    }
    
    public boolean update(Dog dog) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("UPDATE dog SET name=?, breed=?, birthdate=? WHERE id=?");

            stmt.setString(1, dog.getName());
            stmt.setString(2, dog.getBreed());
            stmt.setDate(3, Date.valueOf(dog.getBirthdate()));
            stmt.setInt(4, dog.getId());
            
            return stmt.executeUpdate() == 1;
            
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }

    public boolean add(Dog dog) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO dog (name,breed,birthdate) VALUES (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, dog.getName());
            stmt.setString(2, dog.getBreed());
            stmt.setDate(3, Date.valueOf(dog.getBirthdate()));
            
            if(stmt.executeUpdate() == 1) {
                //Ici, on choppe l'id auto incrémenté et on l'assigne à notre chien
                ResultSet rsKeys = stmt.getGeneratedKeys();
                rsKeys.next();
                dog.setId(rsKeys.getInt(1));
                return true;
            }
            
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }
}
