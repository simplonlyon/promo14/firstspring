package co.simplon.p14.firstspring.entity;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;

public class Dog {
    private Integer id;
    @NotBlank
    private String name;
    private String breed;
    @PastOrPresent
    private LocalDate birthdate;
    public Dog(String name, String breed, LocalDate birthdate) {
        this.name = name;
        this.breed = breed;
        this.birthdate = birthdate;
    }
    public Dog(Integer id, String name, String breed, LocalDate birthdate) {
        this.id = id;
        this.name = name;
        this.breed = breed;
        this.birthdate = birthdate;
    }
    public Dog() {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getBreed() {
        return breed;
    }
    public void setBreed(String breed) {
        this.breed = breed;
    }
    public LocalDate getBirthdate() {
        return birthdate;
    }
    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }
}
